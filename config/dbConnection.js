var mongo = require("mongodb").MongoClient;
var assert = require("assert");
const url = "mongodb+srv://zenonsof:zenfilho090988@cluster0-vlw7g.mongodb.net/test?retryWrites=true&w=majority";
const opt = {
    useUnifiedTopology: true
}
const dbName = "faculdade";
var connMongoDB = function(dados) {
    mongo.connect(url,opt, function(err, client) {            
        assert.equal(null, err);
        console.log("Connected successfully to server");
        const db = client.db(dbName);
        query(db, dados);
        client.close();
    });
};
function query(db, dados) {
    var collection = db.collection(dados.collection);
    switch (dados.operacao) {
        case "inserir":               
            collection.insertOne(dados.usuario, dados.callback);
            break;
        case "find":            
            collection.find(dados.usuario,dados.callback);
            break;
        case "findAll":
            collection.find({},dados.callback);
            break;
        case "findId":
            var ObjectId = require("mongodb").ObjectId;
            collection.find(new ObjectId(dados.parametros),dados.callback);
            break;
        case "atualizar":
            var ObjectId = require("mongodb").ObjectId;
            //updateOne({_id: ObjectId("59ab46e433959e2724be2cbd")}, {$set: {idade: 28}})
            collection.updateOne({_id: ObjectId(dados.id)}, {$set:dados.usuario}, dados.callback);
            break;
        default:
            break;
        case "delete":
            var ObjectId = require("mongodb").ObjectId;
            collection.deleteOne({_id: ObjectId(dados.id)},dados.callback);
            break;
    }
}
module.exports = function() {
    return connMongoDB;
};
    