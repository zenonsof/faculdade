module.exports.index = function(application,req,res){
    if(req.session.autorizado!==true){
        //res.render('index',{alerta:{tipo:"danger",mensagem:"Você precisa fazer login para acessar essa Página. "}});
        res.redirect('login');    
    }else{
        res.render('index');
    }
    
}
module.exports.autenticar = function(application,req,res){
    var dadosForm = req.body;
    var connection = application.config.dbConnection;
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection);
    UsuariosDAO.autenticar(dadosForm,req,res);
}
module.exports.sair = function(application,req,res){
    req.session.destroy(function(){
        res.redirect('login');
    });
}