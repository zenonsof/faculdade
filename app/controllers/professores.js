module.exports.listar = function(application, req, res){
    var connection = application.config.dbConnection;
    var ProfessoresDAO = new application.app.models.ProfessoresDAO(connection);

    ProfessoresDAO.listar(req, res);
}
module.exports.cadastrar = function(application, req, res){

    var dadosForm = req.body;
    
    var connection = application.config.dbConnection;
    var ProfessoresDAO = new application.app.models.ProfessoresDAO(connection);

    ProfessoresDAO.inserirProfessor(dadosForm);
    //geração de parâmetros
}
module.exports.buscarId = function(application, req, res){
    var connection = application.config.dbConnection;
    var ProfessoresDAO = new application.app.models.ProfessoresDAO(connection);
    ProfessoresDAO.buscarId(req, res);    
}
module.exports.atualizar = function(application, req, res){
    var dadosForm = req.body;    
    var connection = application.config.dbConnection;
    var ProfessoresDAO = new application.app.models.ProfessoresDAO(connection);
    ProfessoresDAO.atualizar(dadosForm,req,res);    

}
module.exports.delete = function(application, req, res,idUsuario){
    
    var connection = application.config.dbConnection;
    var ProfessoresDAO = new application.app.models.ProfessoresDAO(connection);
    ProfessoresDAO.delete(req, res);
    
}
module.exports.disciplinas = [
    {
        "cod": "1",
        "nome":"Programação Orientada A objetos"
    },
    {
        "cod": "2",
        "nome":"Segurança em Informação"
    },
    {
        "cod": "3",
        "nome":"Introdução a redes"
    },
];