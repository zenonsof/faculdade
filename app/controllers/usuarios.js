module.exports.usuarios = function(application){
	application.get('/usuarios', function(req, res){
		application.app.controllers.usuarios(application,req,res);
	});
}

module.exports.cadastrar = function(application, req, res){
    
    var dadosForm = req.body;
    
    var connection = application.config.dbConnection;
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection);

    UsuariosDAO.inserirUsuario(dadosForm);
    //geração de parâmetros
}
module.exports.listar = function(application, req, res){
    if(req.session.autorizado!==true){
        res.redirect('login');    
    }else{
        var connection = application.config.dbConnection;
        var UsuariosDAO = new application.app.models.UsuariosDAO(connection);
        UsuariosDAO.listar(req, res);
    }
}
module.exports.buscarId = function(application, req, res,idUsuario){
    
    var connection = application.config.dbConnection;
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection);
    UsuariosDAO.buscarId(idUsuario,req, res);
    
}
module.exports.atualizar = function(application, req, res){
    
    var dadosForm = req.body;
    
    var connection = application.config.dbConnection;
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection);

    UsuariosDAO.atualizar(dadosForm,req,res);    

}
module.exports.delete = function(application, req, res,idUsuario){
    
    var connection = application.config.dbConnection;
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection);
    UsuariosDAO.delete(idUsuario,req, res);
    
}
