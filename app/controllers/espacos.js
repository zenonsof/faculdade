module.exports.listar = function(application, req, res){
    if(req.session.autorizado!==true){
        res.redirect('login');    
    }else{
        var connection = application.config.dbConnection;
        var EspacosDAO = new application.app.models.EspacosDAO(connection);
        EspacosDAO.listar(application,req, res);
    }
}
module.exports.cadastrar = function(application, req, res){

    var dadosForm = req.body;
    
    var connection = application.config.dbConnection;
    var EspacosDAO = new application.app.models.EspacosDAO(connection);

    EspacosDAO.inserir(dadosForm);
    //geração de parâmetros
}
module.exports.buscarId = function(application, req, res,tipoEspaco){
    var connection = application.config.dbConnection;
    var EspacosDAO = new application.app.models.EspacosDAO(connection);
    EspacosDAO.buscarId(req, res,tipoEspaco);    
}
module.exports.atualizar = function(application, req, res){
    var dadosForm = req.body;    
    var connection = application.config.dbConnection;
    var EspacosDAO = new application.app.models.EspacosDAO(connection);
    EspacosDAO.atualizar(dadosForm,req,res);    

}
module.exports.delete = function(application, req, res){
    
    var connection = application.config.dbConnection;
    var EspacosDAO = new application.app.models.EspacosDAO(connection);
    EspacosDAO.delete(req, res);
    
}
module.exports.tipoEspaco = [
    {
        "cod": "1",
        "nome":"Sala"
    },
    {
        "cod": "2",
        "nome":"Laboratório"
    },
    {
        "cod": "3",
        "nome":"Auditório"
    },
];