module.exports.listar = function(application, req, res){
    if(req.session.autorizado!==true){
        res.redirect('login');    
    }else{
        var connection = application.config.dbConnection;
        var AulasDAO = new application.app.models.AulasDAO(connection);
        AulasDAO.listar(application,req, res);
    }
}
module.exports.cadastrar = function(application, req, res){

    var dadosForm = req.body;    
    var connection = application.config.dbConnection;
    var AulasDAO = new application.app.models.AulasDAO(connection);

    AulasDAO.inserir(dadosForm,res);
    //geração de parâmetros
}
module.exports.buscarId = function(application, req, res,tipoEspaco){
    var connection = application.config.dbConnection;
    var AulasDAO = new application.app.models.AulasDAO(connection);
    AulasDAO.buscarId(req, res,tipoEspaco);    
}
module.exports.atualizar = function(application, req, res){
    var dadosForm = req.body;    
    var connection = application.config.dbConnection;
    var AulasDAO = new application.app.models.AulasDAO(connection);
    AulasDAO.atualizar(dadosForm,req,res);    

}
module.exports.delete = function(application, req, res){
    
    var connection = application.config.dbConnection;
    var AulasDAO = new application.app.models.AulasDAO(connection);
    AulasDAO.delete(req, res);
    
}
module.exports.diasSemana = [
    {
        "cod": "1",
        "nome":"Segunda"
    },
    {
        "cod": "2",
        "nome":"Terça"
    },
    {
        "cod": "3",
        "nome":"Quarta"
    },{
        "cod": "4",
        "nome":"Quinta"
    },{
        "cod": "5",
        "nome":"Sexta"
    },
];