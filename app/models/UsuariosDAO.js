//importar módulo do crypto
var crypto = require('crypto');

function UsuariosDAO(connection) {
    this._connection = connection;
}
UsuariosDAO.prototype.inserirUsuario = function(usuario, res) {    
    usuario.senha = crypto.createHash('md5').update(usuario.senha).digest("hex");
    var dados = {
        operacao: "inserir",
        usuario: usuario,
        collection: "usuarios",
        callback: function(err, result) {        
            //res.render("/cadastro");
        }
    };
    this._connection(dados);
};
UsuariosDAO.prototype.autenticar = function(usuario,req,res){
    usuario.senha = crypto.createHash('md5').update(usuario.senha).digest("hex");
    var dados = {
        operacao: "find",
        usuario: usuario,
        collection: "usuarios",
        callback: function(err, result) {        
            result.toArray(function(err2,result2){ 
                if(err2){console.log(err2)}
                if(result2[0]!=undefined){                
                    req.session.autorizado =true;            
                    req.session._id = result2[0]['_id'];        
                    req.session.usuario = result2[0]['nome'];
                    req.session.tipo = result2[0]['tipo'];                    
                }
                if(req.session.autorizado){
                    //quando o usuário é logado, será redirecionado para página jogo
                    res.render("index");
                }else{
                    //quando não encontrado, continua na index
                    res.render('login');
                }
            });
        }
    };
    this._connection(dados);
}
UsuariosDAO.prototype.listar = function(req,res){
    var dados = {
        operacao: "findAll",
        collection: "usuarios",
        callback: function(err, result) {        
            result.sort({ nome: 1 }).toArray(function(err,result2){
                res.render('usuarios/usuarios',{lista:result2});                
            });
        }
    };
    this._connection(dados);
}
UsuariosDAO.prototype.buscarId = function(id,req,res){
    var dados = {
        operacao: "findId",
        parametros: id,
        collection: "usuarios",
        callback: function(err, result) {        
            result.toArray(function(err,result2){ 
                //res.send(result2)
                res.render('./usuarios/editar_usuario', { result:result2[0]});     
            });
        }
    };
    this._connection(dados);
}
UsuariosDAO.prototype.atualizar = function(usuario,req,res){
    idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "atualizar",
        usuario: usuario,
        collection: "usuarios",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
UsuariosDAO.prototype.delete = function(idUsuario,req,res){
    //idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "delete",
        collection: "usuarios",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
module.exports = function() {    
    return UsuariosDAO;
};
    