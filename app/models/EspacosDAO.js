function EspacosDAO(connection) {

    this._connection = connection;
}
EspacosDAO.prototype.inserir= function(usuario, res) {
    var dados = {
        operacao: "inserir",
        usuario: usuario,
        collection: "espacos",
        callback: function(err, result) {        
            //res.render("/cadastro");
        }
    };
    this._connection(dados);
};
EspacosDAO.prototype.autenticar = function(usuario,req,res){
    var dados = {
        operacao: "find",
        usuario: usuario,
        collection: "espacos",
        callback: function(err, result) {        
            result.toArray(function(err2,result2){ 
                if(err2){console.log(err2)}
                if(result2[0]!=undefined){                    
                    req.session.autorizado =true;
                    req.session.usuario = result2[0]['nome'];
                    req.session.tipo = result2[0]['tipo'];
                }
                if(req.session.autorizado){
                    //quando o usuário é logado, será redirecionado para página jogo
                    res.render("index");
                }else{
                    //quando não encontrado, continua na index
                    res.render('login');
                }
            });
        }
    };
    this._connection(dados);
}
EspacosDAO.prototype.listar = function(application,req,res){
    var tipoEspaco = application.app.controllers.espacos.tipoEspaco;
    var dados = {
        operacao: "findAll",
        collection: "espacos",
        callback: function(err, result) {        
            result.sort({ nome: 1 }).toArray(function(err,result2){
                res.render('espacos/espacos',{lista:result2,tipoEspaco:tipoEspaco});                
            });
        }
    };
    this._connection(dados);
}
EspacosDAO.prototype.buscarId = function(req,res,tipoEspaco){
    id=req.params.id;
    var dados = {
        operacao: "findId",
        parametros: id,
        collection: "espacos",
        callback: function(err, result) {        
            result.toArray(function(err,result2){ 
                //res.send(result2)
                
                res.render('./espacos/editar_espaco', { result:result2[0],tipoEspaco:tipoEspaco});     
            });
        }
    };
    this._connection(dados);
}
EspacosDAO.prototype.atualizar = function(usuario,req,res){
    idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "atualizar",
        usuario: usuario,
        collection: "espacos",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
EspacosDAO.prototype.delete = function(req,res){
    id=req.params.id;
    var dados = {
        id: id,
        operacao: "delete",
        collection: "espacos",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
module.exports = function() {    
    return EspacosDAO;
};
    