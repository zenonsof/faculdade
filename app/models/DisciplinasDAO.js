function DisciplinasDAO(connection) {

    this._connection = connection;
}
DisciplinasDAO.prototype.inserir= function(usuario, res) {
    var dados = {
        operacao: "inserir",
        usuario: usuario,
        collection: "disciplinas",
        callback: function(err, result) {        
            //res.render("/cadastro");
        }
    };
    this._connection(dados);
};
DisciplinasDAO.prototype.listar = function(application,req,res){
    //var tipoEspaco = application.app.controllers.disciplinas.tipoEspaco;
    var dados = {
        operacao: "findAll",
        collection: "disciplinas",
        callback: function(err, result) {        
            result.sort({ nome: 1 }).toArray(function(err,result2){
                res.render('disciplinas/disciplinas',{lista:result2});                
            });
        }
    };
    this._connection(dados);
}
DisciplinasDAO.prototype.buscarId = function(req,res){
    id=req.params.id;
    var dados = {
        operacao: "findId",
        parametros: id,
        collection: "disciplinas",
        callback: function(err, result) {        
            result.toArray(function(err,result2){ 
                //res.send(result2)
                
                res.render('./disciplinas/editar_disciplina', { result:result2[0]});     
            });
        }
    };
    this._connection(dados);
}
DisciplinasDAO.prototype.atualizar = function(usuario,req,res){
    idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "atualizar",
        usuario: usuario,
        collection: "disciplinas",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
DisciplinasDAO.prototype.delete = function(req,res){
    id=req.params.id;
    var dados = {
        id: id,
        operacao: "delete",
        collection: "disciplinas",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
module.exports = function() {    
    return DisciplinasDAO;
};
    