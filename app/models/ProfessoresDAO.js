function ProfessoresDAO(connection) {

    this._connection = connection;
}
ProfessoresDAO.prototype.listar = function(req,res){
    var dados = {
        operacao: "findAll",
        collection: "professores",
        callback: function(err, result) {        
            result.sort({ nome: 1 }).toArray(function(err,result2){
                res.render('professores/professores',{lista:result2,alerta:''});                
            });
        }
    };
    this._connection(dados);
}
ProfessoresDAO.prototype.inserirProfessor = function(professor, res) {
    var dados = {
        operacao: "inserir",
        usuario: professor,
        collection: "professores",
        callback: function(err, result) {        
            //res.render("/cadastro");
        }
    };
    this._connection(dados);
};
ProfessoresDAO.prototype.buscarId = function(req,res){
    var idProfessor = req.params.id;
    var dados = {
        operacao: "findId",
        parametros: idProfessor,
        collection: "professores",
        callback: function(err, result) {        
            result.toArray(function(err,result2){ 
                //res.send(result2)
                res.render('./professores/editar_professor', { result:result2[0]});     
            });
        }
    };
    this._connection(dados);
}
ProfessoresDAO.prototype.atualizar = function(usuario,req,res){
    idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "atualizar",
        usuario: usuario,
        collection: "professores",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
ProfessoresDAO.prototype.delete = function(req,res){
    idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "delete",
        collection: "professores",
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
module.exports = function() {    
    return ProfessoresDAO;
};