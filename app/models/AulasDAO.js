function AulasDAO(connection) {
    this.collection = "aulas"
    this._connection = connection;
}
AulasDAO.prototype.inserir= function(usuario, res) {
    
    var ObjectId = require("mongodb").ObjectId;
    idCoordenador = new ObjectId(usuario.idCoordenador);
    nomeCoordenador = usuario.nomeCorrdenador;
    delete(usuario.idCoordenador);
    delete(usuario.nomeCorrdenador);
    usuario.coordenador = {
        _id:idCoordenador,
        nome:nomeCoordenador
    }    
    var dados = {
        operacao: "inserir",
        usuario: usuario,
        collection: this.collection,
        callback: function(err, result) {        
            //res.render("/cadastro");
        }
    };
    this._connection(dados);
};
AulasDAO.prototype.listar = function(application,req,res){
    var diasSemana = application.app.controllers.aulas.diasSemana.map(item => [item.cod,item.nome]);
    var dados = {
        operacao: "findAll",
        collection: this.collection,
        callback: function(err, result) {        
            result.sort({ nome: 1 }).toArray(function(err,result2){
                res.render('aulas/aulas',{lista:result2,session:req.session,diasSemana:diasSemana});                
            });
        }
    };
    this._connection(dados);
}
AulasDAO.prototype.buscarId = function(req,res){
    id=req.params.id;
    var dados = {
        operacao: "findId",
        parametros: id,
        collection: this.collection,
        callback: function(err, result) {        
            result.toArray(function(err,result2){ 
                //res.send(result2)
                
                res.render('./aulas/editar_turma', { result:result2[0]});     
            });
        }
    };
    this._connection(dados);
}
AulasDAO.prototype.atualizar = function(usuario,req,res){
    idUsuario=req.params.id;
    var dados = {
        id: idUsuario,
        operacao: "atualizar",
        usuario: usuario,
        collection: this.collection,
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
AulasDAO.prototype.delete = function(req,res){
    id=req.params.id;
    var dados = {
        id: id,
        operacao: "delete",
        collection: this.collection,
        callback: function(err, result) {                 
            if(err){console.log(err)}
        }
    };
    this._connection(dados);
}
module.exports = function() {    
    return AulasDAO;
};
    