module.exports = function(application){
	application.get('/', function(req, res){
		application.app.controllers.index.index(application,req,res);
	});

	application.get('/login', function(req, res){
		res.render('login')
	});
	application.post('/autenticar', function(req, res){
		application.app.controllers.index.autenticar(application,req,res);
	});
	application.get('/sair', function(req, res){
		application.app.controllers.index.sair(application,req,res);
	});
}