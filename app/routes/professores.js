module.exports = function(application){
	application.get('/professores', function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			application.app.controllers.professores.listar(application,req,res);	
		}
    });
    application.get('/add_professor', function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			var disciplinas = application.app.controllers.professores.disciplinas;
			application.app.controllers.professores.listar(application,req,res);
			res.render("professores/add_professor",{disciplinas:disciplinas});
		}
	});
	application.post('/cadastrar_professor', function(req, res){
		application.app.controllers.professores.cadastrar(application,req,res);
		application.app.controllers.professores.listar(application,req,res);
		res.redirect("/professores");
	});

	application.get('/professores/:id', function(req, res){
		//var idUsuario = req.params.id;
		application.app.controllers.professores.buscarId(application,req,res);
	});
	application.post('/professores/:id', function(req, res){	
		application.app.controllers.professores.atualizar(application,req,res);
		application.app.controllers.professores.listar(application,req,res);
		var alerta = "Item <strong>Salvo</strong> com sucesso."
		res.redirect("/professores");
		
	});

	application.get('/professores/delete/:id', function(req, res){
		//var id = req.params.id;
		application.app.controllers.professores.delete(application,req,res);
		application.app.controllers.professores.listar(application,req,res);
		res.redirect("/professores");
	});
}