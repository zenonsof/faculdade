module.exports = function(application){
	application.get('/usuarios', function(req, res){
		//res.render("usuarios/usuarios")
		application.app.controllers.usuarios.listar(application,req,res);	
	});
	application.get('/add_usuario', function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			res.render("usuarios/add_usuario");
		}
	});
	application.post('/cadastrar', function(req, res){
		application.app.controllers.usuarios.cadastrar(application,req,res);
		application.app.controllers.usuarios.listar(application,req,res);
		res.redirect("/usuarios");
	});
	//application.get('/usuarios/editar/:id', function(req, res){
	application.get('/usuarios/:id', function(req, res){
		var idUsuario = req.params.id;
		application.app.controllers.usuarios.buscarId(application,req,res,idUsuario);
	});
	application.get('/usuarios/delete/:id', function(req, res){
		var idUsuario = req.params.id;
		application.app.controllers.usuarios.delete(application,req,res,idUsuario);
		application.app.controllers.usuarios.listar(application,req,res);
		res.redirect("/usuarios");
	});
	application.post('/usuarios/:id', function(req, res){	
		var idUsuario = req.params.id;
		application.app.controllers.usuarios.atualizar(application,req,res);
		application.app.controllers.usuarios.listar(application,req,res);
		res.render("/usuarios");
	});
}

