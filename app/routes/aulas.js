module.exports = function(application){
	var modulos = "aulas";
	var modulo = "aula";
	application.get('/'+modulos, function(req, res){
		application.app.controllers.aulas.listar(application,req,res);	
    });
    application.get('/add_'+modulo, function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			//var tipodisciplina = application.app.controllers.aulas.tipodisciplina;
			//application.app.controllers.aulas.listar(application,req,res);		
			res.render(modulos+"/add_"+modulo,{session:req.session});
		}
	});
	application.post('/cadastrar_'+modulo, function(req, res){
		application.app.controllers.aulas.cadastrar(application,req,res);
		application.app.controllers.aulas.listar(application,req,res);
		res.redirect("/"+modulos);
	});

	application.get('/'+modulos+'/:id', function(req, res){
		//var idUsuario = req.params.id;
		application.app.controllers.aulas.buscarId(application,req,res);
	});
	application.post('/'+modulos+'/:id', function(req, res){			
		application.app.controllers.aulas.atualizar(application,req,res);
		application.app.controllers.aulas.listar(application,req,res);		
		res.redirect("/"+modulos);
		
	});

	application.get('/'+modulos+'/delete/:id', function(req, res){
		//var id = req.params.id;
		application.app.controllers.aulas.delete(application,req,res);
		application.app.controllers.aulas.listar(application,req,res);
		res.redirect("/"+modulos);
	});
}