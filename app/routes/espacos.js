module.exports = function(application){
	application.get('/espacos', function(req, res){
		application.app.controllers.espacos.listar(application,req,res);	
    });
    application.get('/add_espaco', function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			var tipoEspaco = application.app.controllers.espacos.tipoEspaco;
			application.app.controllers.espacos.listar(application,req,res);
			res.render("espacos/add_espaco",{tipoEspaco:tipoEspaco});
		}
	});
	application.post('/cadastrar_espaco', function(req, res){
		application.app.controllers.espacos.cadastrar(application,req,res);
		application.app.controllers.espacos.listar(application,req,res);
		res.redirect("/espacos");
	});

	application.get('/espacos/:id', function(req, res){
		//var idUsuario = req.params.id;
		var tipoEspaco = application.app.controllers.espacos.tipoEspaco;
		application.app.controllers.espacos.buscarId(application,req,res,tipoEspaco);
	});
	application.post('/espacos/:id', function(req, res){			
		application.app.controllers.espacos.atualizar(application,req,res);
		application.app.controllers.espacos.listar(application,req,res);		
		res.redirect("/espacos");
		
	});

	application.get('/espacos/delete/:id', function(req, res){
		//var id = req.params.id;
		application.app.controllers.espacos.delete(application,req,res);
		application.app.controllers.espacos.listar(application,req,res);
		res.redirect("/espacos");
	});
}