module.exports = function(application){
	application.get('/disciplinas', function(req, res){
		application.app.controllers.disciplinas.listar(application,req,res);	
    });
    application.get('/add_disciplina', function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			//var tipodisciplina = application.app.controllers.disciplinas.tipodisciplina;
			application.app.controllers.disciplinas.listar(application,req,res);
			//res.render("disciplinas/add_disciplina",{tipodisciplina:tipodisciplina});
			res.render("disciplinas/add_disciplina");
		}
	});
	application.post('/cadastrar_disciplina', function(req, res){
		application.app.controllers.disciplinas.cadastrar(application,req,res);
		application.app.controllers.disciplinas.listar(application,req,res);
		res.redirect("/disciplinas");
	});

	application.get('/disciplinas/:id', function(req, res){
		//var idUsuario = req.params.id;
		application.app.controllers.disciplinas.buscarId(application,req,res);
	});
	application.post('/disciplinas/:id', function(req, res){			
		application.app.controllers.disciplinas.atualizar(application,req,res);
		application.app.controllers.disciplinas.listar(application,req,res);		
		res.redirect("/disciplinas");
		
	});

	application.get('/disciplinas/delete/:id', function(req, res){
		//var id = req.params.id;
		application.app.controllers.disciplinas.delete(application,req,res);
		application.app.controllers.disciplinas.listar(application,req,res);
		res.redirect("/disciplinas");
	});
}