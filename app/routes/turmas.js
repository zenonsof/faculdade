module.exports = function(application){
	var modulos = "turmas";
	var modulo = "turma";
	application.get('/'+modulos, function(req, res){
		application.app.controllers.turmas.listar(application,req,res);	
    });
    application.get('/add_'+modulo, function(req, res){
		if(req.session.autorizado!==true){
			res.redirect('login');    
		}else{
			//var tipodisciplina = application.app.controllers.turmas.tipodisciplina;
			//application.app.controllers.turmas.listar(application,req,res);		
			res.render(modulos+"/add_"+modulo,{session:req.session});
		}
	});
	application.post('/cadastrar_'+modulo, function(req, res){
		application.app.controllers.turmas.cadastrar(application,req,res);
		application.app.controllers.turmas.listar(application,req,res);
		res.redirect("/"+modulos);
	});

	application.get('/'+modulos+'/:id', function(req, res){
		//var idUsuario = req.params.id;
		application.app.controllers.turmas.buscarId(application,req,res);
	});
	application.post('/'+modulos+'/:id', function(req, res){			
		application.app.controllers.turmas.atualizar(application,req,res);
		application.app.controllers.turmas.listar(application,req,res);		
		res.redirect("/"+modulos);
		
	});

	application.get('/'+modulos+'/delete/:id', function(req, res){
		//var id = req.params.id;
		application.app.controllers.turmas.delete(application,req,res);
		application.app.controllers.turmas.listar(application,req,res);
		res.redirect("/"+modulos);
	});
}